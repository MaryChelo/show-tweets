# Show Tweets

El objetivo de esta aplicación es extraer un numero de Tweets de algun usuario en esepcifico, posteriormente depurando el contenido más relevante y almacenarlo en una base de datos MySQL.

## Contenido
 * [Requisitos de instalacón](#requisitos-de-instalac%C3%B3n)
 * [Tecnologías usadas](#tecnolog%C3%ADas-usadas)
 * [Configuración](#configuraci%C3%B3n)
    * [Incluida](#incluida)
    * [Necesaria](#necesaria)
    * [Opcional](#opcional)
 * [Instrucciones de Uso](#instrucciones-de-uso)

## Requisitos de instalacón

Para hacer uso de esta aplicación es necesario contar con los requisitos mínimos enlistados a continuación:

Software | Versión
-------- | -------
JDK      |    1.7
MySQL    |    5.7
Netbeans |   8.0.2

Para la instalación del JDK 7 se puede buscar el [Java Developement Kit 7 + NetBeans 8.0.2 bundle](https://www.oracle.com/technetwork/java/javase/downloads/jdk-7-netbeans-download-432126.html). 

Para descargar MySQL podemos dirigirnos a la [página oficial](https://dev.mysql.com/downloads/mysql/5.7.html#downloads); es importante no olvidar asignar una contraseña a root en la instalación.

### Nota:
* No se ha probado con el JDK 9 o posterior, por lo cual no se asegura su funcionalidad con esas versiones.

## Tecnologías usadas

Para el desarrollo de esta aplicación se hizo uso de las siguientes tecnologías:
* [Java Developement Kit 7](https://www.oracle.com/technetwork/es/java/javase/downloads/jdk7-downloads-1880260.html)
* [Twitter API version 4](http://twitter4j.org/en/)
* [MySLQ Connector version 5.1](https://dev.mysql.com/downloads/connector/j/8.0.html)

El proyecto ya incluye las librerias necesarias para su ejecución; en caso de no marcar como ausentes, agregarlas desede el menu de NetBeans como se puede ver a continuación:

* Dar Click derecho sobre la carpeta de Libraries y seleccionar la opción de Add JAR/Folder

![dsd](/images/add%20library1.png)

* Buscar la libreria que se desea agregar y seleccionar OK 

![dsd](/images/add%20library2.png)

##  Configuración 
### Incluida
La aplicación ya cuenta con la autorización para acceder a Twitter, las llaves se encuentran en el archivo [auth](/src/auth/auth_file.txt)

### Necesaria 
Para poder ejecutar esta aplicación solo es necesario realizar un pequeño cambio en la clase [MySqlConn](/src/BD/MySqlConn.java) en la linea siguiente:
```java
    connectionUrl = "jdbc:mysql://localhost:3306/twitter?"
                    +"user=root&password=moonsun9";
```
Sustituyendo el local host con la direccion ip y el puerto del servidor de base de datos; la base de datos se queda igual, y cambia el usuario y la contraseña para tener acceso a la base de datos.

Levantar la base de datos con el script sql [Base de Datos](BD.sql)

### Opcional
En caso de no autorizar al usuario default de la aplicación será necesario que realice la autorización para obtener el PIN e ingresarlo en la siguiente ventana emergente de la aplicación. 

![dsd](/images/Tweet3.png)

## Instrucciones de Uso

La pantalla principal se muestra de la siguiente manera:

![dsd](/images/Tweet1.png)

La cual se encuentra a la espera de un nombre de usuario de Twitter y presionar actualizar.

![dsd](/images/Tweet2.png)

### Nota:
* La pantalla puede tardar en actualizarse, ya que hace al mismo tiempo la inserción a la base de datos.





